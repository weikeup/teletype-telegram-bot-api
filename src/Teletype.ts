import Axios, { AxiosResponse } from 'axios'
import { Update, Message, User } from 'telegram-typings'
import { TgEvents as _TgEvents } from './TgEvents'

export const TgEvents = _TgEvents

export const enum ParseMode {
    MarkdownV2 = "MarkdownV2",
    HTML = "HTML",
    Markdown = "Markdown"
}

export class Teletype {
    private _token = ""
    private _botURL = ""
    private events = new Map<_TgEvents, Map<string, (update: Update) => void | Promise<void>>>()

    get token(): string {
        return this._token
    }


    get botURL(): string {
        return this._botURL
    }

    constructor(token: string) {
        this._token = token
        this._botURL = `https://api.telegram.org/bot${token}`
    }

    async getUpdates(timeout: number = 1800): Promise<Array<Update>> {
        let updates: Array<Update> | undefined
        try {
            const res = await Axios.post(
                `${this._botURL}/getUpdates`,
                {
                    timeout: timeout,
                },
                {
                    timeout: (timeout + 30) * 1000
                }
            )
            updates = res.data.result as Array<Update>
            if (updates.length > 0) {
                const maxUpdateId = updates[updates.length - 1].update_id
                await Axios.post(
                    `${this._botURL}/getUpdates`,
                    {
                        timeout: 0,
                        offset: maxUpdateId + 1
                    }
                )
            }
        } catch (err) {
            console.error(err.message);
        }
        return updates || []
    }

    async getMe(): Promise<User | null> {
        try {
            const res = await Axios.get(`${this._botURL}/getMe`)
            return res.data.result as User
        } catch (err) {
            console.error(err.message);
        }
        return null
    }

    async setWebhook(url: string): Promise<boolean> {
        let res: AxiosResponse<any> | undefined
        try {
            res = await Axios.post(
                `${this._botURL}/setWebhook`,
                {
                    url: url
                }
            )
        } catch (err) {
            console.error(err.message);
        }
        return res?.data.result as boolean || false
    }

    async deleteWebhook(): Promise<boolean> {
        let res: AxiosResponse<any> | undefined
        try {
            res = await Axios.get(`${this._botURL}/deleteWebhook`)
        } catch (err) {
            console.error(err.message);
        }
        return res?.data.result as boolean || false
    }

    on(event: _TgEvents, eventId: string, run: (update: Update) => void | Promise<void>) {
        if (!this.events.has(event)) {
            this.events.set(event, new Map())
        }
        const runs = this.events.get(event)
        runs?.set(eventId, run)
    }

    onText(regStr: string, run: (message: Message, update: Update) => void | Promise<void>) {
        this.on(TgEvents.TEXT, regStr, async (update: Update) => {
            if (update.message?.text !== undefined) {
                if (new RegExp(regStr).test(update.message.text)) {
                    await run(update.message, update)
                }
            }
        })
    }

    onCommand(cmdStr: string, run: (message: Message, update: Update) => void | Promise<void>) {
        this.on(TgEvents.TEXT, cmdStr, async (update: Update) => {
            if (update.message?.text !== undefined && update.message.entities?.some((value) => { return value.type === "bot_command" })) {
                if (new RegExp(`^/${cmdStr}`).test(update.message.text)) {
                    await run(update.message, update)
                }
            }
        })
    }

    always(run: (update: Update) => void | Promise<void>) {
        this.on(TgEvents.ALWAYS, "default_always", run)
    }

    delEvent(event: _TgEvents, eventId: string): boolean {
        return this.events.get(event)?.delete(eventId) || false
    }

    async processUpdate(update: Update) {
        for (const eventMap of this.events) {
            if (update.message?.[eventMap[0]] !== undefined || eventMap[0] === TgEvents.ALWAYS) {
                for (const event of eventMap[1]) {
                    await event[1](update)
                }
            }
        }
    }

    async sendMessage(
        chatId: number | string,
        text: string,
        option?: {
            parse_mode?: ParseMode
            disable_web_page_preview?: boolean,
            disable_notification?: boolean,
            replay_to_message_id?: number,
        }
    ) {
        try {
            const postData = {
                text: text,
                chat_id: chatId
            }
            if (option !== undefined) {
                Object.assign(postData, option)
            }
            await Axios.post(`${this._botURL}/sendMessage`, postData)
        } catch (err) {
            console.error(err.message)
        }
    }

    async sendSticker(
        chatId: number | string,
        stickerFileId: string,
        option?: {
            disable_notification?: boolean,
            reply_to_message_id?: number,
        }
    ) {
        try {
            const postData = {
                chat_id: chatId,
                sticker: stickerFileId
            }
            if (option !== undefined) {
                Object.assign(postData, option)
            }
            await Axios.post(`${this._botURL}/sendSticker`, postData)
        } catch (err) {
            console.error(err.message)
        }
    }
}